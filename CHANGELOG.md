## [Unreleased]

## [1.1.0] - 2023-12-07

### Changed

- Upgrade development environment to Ruby 3.2
- Compatibility with Faraday 2

## [1.0.0] - 2022-04-04

- Initial release
