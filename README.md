# AmadeusDiscover

Faraday based Ruby client for the Amadeus Discover API for content partners use.

## Installation

`AmadeusDiscover` is distributed as a gem and available on rubygems.org so you can add it to your Gemfile or install it manually with:

````shell
$ gem install amadeus-discover
````

## Usage

### Initializing a new client instance

The client can be initialized directly:

````ruby
connection = Faraday::Connection.new('https://api-test.amadeus-discover.com') do |builder|
  # Add middlewares, a logger or specify an adapter 
end

credentials = {
  username: '{username}',
  password: '{password}',
  client_id: 'content-partner-api',
  grant_type: 'password'
}

client = AmadeusDiscover::Client.new(connection, credentials)
````

Alternatively, you can configure `AmadeusDiscover` to use default connection and credentials for every client instances:

````ruby
# In <config/initializers/amadeus_discover.rb>
AmadeusDiscover.configure do |config|
  config.connection = Faraday::Connection.new(ENV['AMADEUS_DISCOVER_HOST']) do |builder|
    builder.response :logger, Rails.logger
    builder.adapter :typhoeus
  end
  
  config.credentials = {
    username: ENV['AMADEUS_DISCOVER_USERNAME'],
    password: ENV['AMADEUS_DISCOVER_PASSWORD'],
    client_id: 'content-partner-api',
    grant_type: 'password'
  }
end

# Anywhere in your application
client = AmadeusDiscover::Client.new
````

### Using a client

Once created, a client gives you access to Amadeus Discover API methods:

````ruby
client = AmadeusDiscover::Client.new

# Get all suppliers - Not available at this time
# suppliers = client.suppliers.list

# Create a supplier
s = client.suppliers.create(name: 'My supplier')

client.suppliers(s['id']).tap do |supplier|
  # Get supplier informations
  supplier.get
  
  # Update supplier
  supplier.update(more_data)
  
  # Delete supplier
  supplier.delete

  # Get all products for a supplier
  products = supplier.products.list
  
  # Create a new product for a supplier
  p = supplier.products.create(title: { en: "Let's have some fun!" })

  supplier.products(p['id']).tap do |product|
    # Get product informations - Not available at this time
    # supplier.get
    
    # Update product
    product.update(more_data)
    
    # Delete supplier
    product.delete
  end
end
````

If an error occured, a `Faraday::Error` will be raised. The exact type of that exception will depend on the encountered error type (authentication error, client error, server error…). See <https://lostisland.github.io/faraday/middleware/raise-error> for details.

## Contributing

Bug reports and pull requests are welcome on Gitlab at <https://gitlab.com/vaolo-public/ruby/amadeus-discover>.

### Development

After checking out the repo, run `bundle install` to install development dependencies.

Running tests require a bunch of environment variables to be declared, to identify where is hosted the instance of the Amadeus Discover API you'll target and credentials you'll use for authentication. Development tools integrate [dotenv](https://github.com/bkeepers/dotenv) so you can do it in a `.env` file and a `.env.example` is available to list and document all required variables.

Once your `.env` file is ready, run `bundle exec rake spec` to run the tests.
