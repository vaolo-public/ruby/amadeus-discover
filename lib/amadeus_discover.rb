# frozen_string_literal: true

require 'amadeus_discover/version'

# Amadeus Discover API client global namespace
module AmadeusDiscover
  autoload :Authenticator,  'amadeus_discover/authenticator'
  autoload :Config,         'amadeus_discover/config'
  autoload :Client,         'amadeus_discover/client'
  autoload :Namespace,      'amadeus_discover/namespace'
  autoload :Namespaces,     'amadeus_discover/namespaces'

  class << self
    # Shortcut to globally configure AmadeusDiscover clients
    #
    # Yield the AmadeusDiscover configuration object to a block.
    # See {AmadeusDiscover::Config} for available configurations.
    def configure
      yield AmadeusDiscover::Config
    end
  end
end
