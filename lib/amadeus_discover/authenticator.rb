# frozen_string_literal: true

module AmadeusDiscover
  # Authenticator to be used with Faraday's Authentication Middleware
  #
  # Authentication Middleware and this are automatically injected into your
  # connection stack so you don't need to configure them.
  class Authenticator
    autoload :Credentials,  'amadeus_discover/authenticator/credentials'
    autoload :Token,        'amadeus_discover/authenticator/token'
    autoload :Response,     'amadeus_discover/authenticator/response'

    ENDPOINT = '/auth/realms/amadeus-discover/protocol/openid-connect/token'

    def initialize(connection, credentials = {})
      @connection = reconfigure(connection)
      @credentials = Credentials.new(credentials)
    end

    def bearer_token
      @token = nil unless token.valid?
      token.value
    end

    protected

    attr_reader :connection, :credentials

    def token
      @token ||= fetch_token
    end

    def fetch_token
      response = connection.post(ENDPOINT, credentials.to_hash)

      begin
        Token.new(response.body)
      rescue ArgumentError => e
        raise Faraday::ParsingError.new(e, response)
      end
    end

    # Ensure given connection is always configured with at least what we know we need internally.
    def reconfigure(connection)
      connection.builder.tap do |builder|
        builder.request :url_encoded

        builder.response :json
        builder.response :raise_error
      end

      connection
    end
  end
end
