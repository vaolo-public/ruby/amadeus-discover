# frozen_string_literal: true

require 'hashie'

module AmadeusDiscover
  class Authenticator
    # Store and validate credentials that will be used for authentication.
    #
    # @!visibility private
    class Credentials < Hashie::Dash
      include Hashie::Extensions::IndifferentAccess
      include Hashie::Extensions::IgnoreUndeclared

      property :username, required: true
      property :password, required: true
      property :client_id, required: true
      property :grant_type, required: true
    end
  end
end
