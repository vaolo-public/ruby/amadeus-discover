# frozen_string_literal: true

require 'hashie'

module AmadeusDiscover
  class Authenticator
    # Store and validate an authentication response, as delivered by the
    # authentication API endpoint.
    #
    # @!visibility private
    class Response < Hashie::Dash
      include Hashie::Extensions::IndifferentAccess
      include Hashie::Extensions::IgnoreUndeclared

      property :access_token, required: true
      property :expires_in, required: true
    end
  end
end
