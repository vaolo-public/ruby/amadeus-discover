# frozen_string_literal: true

require 'forwardable'

module AmadeusDiscover
  class Authenticator
    # Handle an authentication token
    #
    # @!visibility private
    class Token
      extend Forwardable

      attr_reader :issued_at

      def_delegators :@response, :access_token, :expires_in
      alias value access_token

      def initialize(response, issued_at = Time.now)
        @response = Response.new(response)
        @issued_at = issued_at
      end

      def valid?
        (issued_at + expires_in) > Time.now
      end
    end
  end
end
