# frozen_string_literal: true

module AmadeusDiscover
  # The Amadeus Discover API client
  class Client
    autoload :Decorator, 'amadeus_discover/client/decorator'

    include Namespaces

    attr_reader :connection

    def initialize(connection = AmadeusDiscover::Config.connection, credentials = AmadeusDiscover::Config.credentials)
      conn = appropriate(connection)
      @connection = reconfigure(conn, AmadeusDiscover::Authenticator.new(clone_connection(conn), credentials))
    end

    protected

    def appropriate(connection)
      conn = clone_connection(connection)
      conn.headers.update(user_agent: user_agent_string(conn.headers[:user_agent]))
      conn
    end

    # Workaround a glitch in Faraday: cloning a Faraday::Connection with only an
    # adapter set but will reset clone builder to Faraday's default middlewares
    # **and adapter**. We must set the adapter again and remove default handlers.
    def clone_connection(original)
      dup = original.dup
      return dup unless original.builder.handlers.empty?

      dup.builder.tap do |builder|
        builder.handlers.each { |handler| builder.delete handler }
        builder.adapter original.builder.adapter.klass
      end

      dup
    end

    # Ensure given connection is always configured with at least what we know we need internally.
    def reconfigure(connection, authenticator)
      connection.builder.tap do |builder|
        builder.request :authorization, 'Bearer', -> { authenticator.bearer_token }
        builder.request :json

        builder.response :json
        builder.response :raise_error
      end

      connection
    end

    def user_agent_string(base)
      [base, "AmadeusDiscover/#{AmadeusDiscover::VERSION}", "Ruby/#{RUBY_VERSION}"].join(' ')
    end
  end
end
