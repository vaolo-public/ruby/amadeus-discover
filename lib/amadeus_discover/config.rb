# frozen_string_literal: true

# Class variables are fine here as this configuration module is not to be used
# in any inheritance or composition chain.
# rubocop:disable Style/ClassVars
module AmadeusDiscover
  # Global configuration for {AmadeusDiscover::Client}s.
  module Config
    # Credentials to be used by default to authenticate client requests.
    #
    # Must be a Hash with valid credentials, client ID and grant type. Ex:
    #
    #     {
    #       username: '{username}',
    #       password: '{password}',
    #       client_id: 'content-partner-api',
    #       grant_type: 'password'
    #     }
    @@credentials = {}

    # A Faraday::Connection to be used by default to query the API.
    #
    # Will be reconfigured on client's initialization to use the correct request
    # and response middlewares, depending on the queried endpoint. Use this to
    # specify API host you want to query, a Faraday adapter, or logger or
    # instrumentation middleware to integrate Amadeus Discover in your
    # application.
    @@connection = nil

    class << self
      def credentials
        @@credentials
      end

      def credentials=(credentials)
        @@credentials = credentials
      end

      def connection
        @@connection
      end

      def connection=(connection)
        @@connection = connection
      end
    end
  end
end
# rubocop:enable Style/ClassVars
