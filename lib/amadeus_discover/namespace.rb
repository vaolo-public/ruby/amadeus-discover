# frozen_string_literal: true

module AmadeusDiscover
  # Parent class for all action namespaces
  #
  # @!visibility private
  class Namespace
    extend Forwardable

    # The API client
    # @return [AmadeusDiscover::Client]
    attr_reader :client

    def_delegators :@client, :connection

    # Initialize the namespaced client with an {AmadeusDiscover::Client}
    # instance.
    # @param [AmadeusDiscover::Client] client
    def initialize(client)
      @client = client
    end
  end
end
