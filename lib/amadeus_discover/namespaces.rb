# frozen_string_literal: true

module AmadeusDiscover
  # Top-level action namespaces, available on instances of
  # AmadeusDiscover::Client.
  module Namespaces
    autoload :Suppliers, 'amadeus_discover/namespaces/suppliers'

    # Get access to suppliers
    def suppliers(supplier_id = nil)
      return Suppliers::Collection.new(self) if supplier_id.nil?

      Suppliers::Member.new(self, supplier_id)
    end
  end
end
