# frozen_string_literal: true

module AmadeusDiscover
  module Namespaces
    # An action namespace for the +/api/supplier+ endpoints.
    module Suppliers
      autoload :Products, 'amadeus_discover/namespaces/suppliers/products'

      # Namespace for actions on suppliers collection.
      class Collection < AmadeusDiscover::Namespace
        # Create a new supplier
        def create(supplier_data)
          connection.post(path, supplier_data).body
        end

        protected

        def path
          '/api/supplier'
        end
      end

      # Namespace for actions on a supplier.
      class Member < AmadeusDiscover::Namespace
        attr_reader :supplier_id

        def initialize(client, supplier_id)
          @supplier_id = supplier_id
          super(client)
        end

        # Retrieve supplier information
        def get
          connection.get(path).body
        end

        # Udpate supplier
        def update(supplier_data)
          connection.post(path, supplier_data).body
        end

        # Delete supplier
        def delete
          connection.delete(path).body
        end

        # Get access to supplier's products
        def products(product_id = nil)
          return Products::Collection.new(self) if product_id.nil?

          Products::Member.new(self, product_id)
        end

        protected

        def path
          "/api/supplier/#{supplier_id}"
        end
      end
    end
  end
end
