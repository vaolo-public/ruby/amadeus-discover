# frozen_string_literal: true

module AmadeusDiscover
  module Namespaces
    module Suppliers
      # An action namespace for the +/api/supplier/:id/product+ endpoints.
      module Products
        # Namespace for actions on a supplier's products collection.
        class Collection < AmadeusDiscover::Namespace
          # Returns the list of products for supplier.
          def list
            connection.get(path).body
          end

          # Create a new product
          def create(product_data)
            # WARN: Can not use path here because URLs do not follow REST conventions.
            connection.post("/api/supplier/#{client.supplier_id}/product", product_data).body
          end

          protected

          def path
            "/api/supplier/#{client.supplier_id}/products"
          end
        end

        # Namespace for actions on a supplier's product.
        class Member < AmadeusDiscover::Namespace
          attr_reader :product_id

          def initialize(client, product_id)
            @product_id = product_id
            super(client)
          end

          # Retrieve product information
          def get
            connection.get(path).body
          end

          # Udpate product
          def update(product_data)
            connection.post(path, product_data).body
          end

          # Delete product
          def delete
            connection.delete(path).body
          end

          protected

          def path
            "/api/supplier/#{client.supplier_id}/product/#{product_id}"
          end
        end
      end
    end
  end
end
