# frozen_string_literal: true

RSpec.describe AmadeusDiscover::Authenticator, type: :integration do
  subject(:authenticator) { described_class.new(connection, auth_params) }

  let(:connection) { http_connection(domain) }

  describe '.bearer_token' do
    context 'with correct credentials' do
      let(:auth_params) { credentials }

      it 'returns a token' do
        expect(authenticator.bearer_token).to be_an(String)
      end
    end

    context 'with bad credentials' do
      let(:auth_params) { credentials(username: 'invalid', password: 'invalid') }

      it 'raise an exception' do
        expect { authenticator.bearer_token }.to raise_error(Faraday::UnauthorizedError)
      end
    end
  end
end
