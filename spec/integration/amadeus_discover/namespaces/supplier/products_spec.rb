# frozen_string_literal: true

RSpec.describe AmadeusDiscover::Namespaces::Suppliers::Products, type: :integration do
  around do |example|
    @supplier = client.suppliers.create(fake_supplier_data)
    example.run
  ensure
    client.suppliers(@supplier['id']).products(@product['id']).delete if instance_variable_defined?(:@product)
    client.suppliers(@supplier['id']).delete
  end

  let(:client) { AmadeusDiscover::Client.new(connection, credentials) }
  let(:connection) { http_connection(domain) }

  let(:product_data) { fake_product_data }
  let(:product_keys) { product_data.keys.collect(&:to_s) + %w[id creationDate onlineBookable] }

  describe 'on collection' do
    subject(:products) { client.suppliers(@supplier['id']).products }

    describe '.create' do
      it 'returns a Hash' do
        @product = products.create(product_data)

        expect(@product).to be_a(Hash)
      end

      it 'returns a Hash that represents a Product' do
        @product = products.create(product_data)

        expect(@product.keys).to include(*product_keys)
      end

      it 'returns a persisted Product' do
        @product = products.create(product_data)

        expect(@product['id']).not_to be_nil
      end

      it 'returns a persisted Product with correct data' do
        @product = products.create(product_data)

        expect(@product['title']['fr']).to eq(product_data[:title][:fr])
      end

      it 'adds a product to supplier inventory' do
        expect do
          @product = products.create(product_data)
        end.to(change { client.suppliers(@supplier['id']).products.list.size }.by(1))
      end
    end
  end

  describe 'on member' do
    subject(:product) { client.suppliers(@supplier['id']).products(@product['id']) }

    before { @product = client.suppliers(@supplier['id']).products.create(product_data) }

    it '.get', skip: 'Not available'

    describe '.update' do
      let(:update_data) { fake_product_data }

      it 'returns a Hash' do
        updated = product.update(update_data)

        expect(updated).to be_a(Hash)
      end

      it 'returns a Hash that represents a Product' do
        updated = product.update(update_data)

        expect(updated.keys).to include(*product_keys)
      end

      it 'returns the same Product' do
        updated = product.update(update_data)

        expect(updated['id']).to eq(@product['id'])
      end

      it 'returns an updated Product' do
        updated = product.update(update_data)

        expect(updated['title']['fr']).to eq(update_data[:title][:fr])
      end

      it 'does not add a product to supplier inventory' do
        expect do
          product.update(update_data)
        end.not_to(change { client.suppliers(@supplier['id']).products.list.size })
      end
    end

    describe '.delete' do
      it 'deletes a Product' do
        product.delete

        expect(client.suppliers(@supplier['id']).products.list).to be_empty
        remove_instance_variable(:@product)
      end

      it 'removes a product from supplier inventory' do
        expect do
          product.delete
        end.to(change { client.suppliers(@supplier['id']).products.list.size }.by(-1))
        remove_instance_variable(:@product)
      end
    end
  end
end
