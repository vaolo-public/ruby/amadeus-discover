# frozen_string_literal: true

RSpec.describe AmadeusDiscover::Namespaces::Suppliers, type: :integration do
  around do |example|
    example.run
  ensure
    client.suppliers(@supplier['id']).delete if instance_variable_defined?(:@supplier)
  end

  let(:client) { AmadeusDiscover::Client.new(connection, credentials) }
  let(:connection) { http_connection(domain) }

  let(:supplier_data) { fake_supplier_data }
  let(:supplier_keys) { supplier_data.keys.collect(&:to_s) + %w[id creationDate onlineBookable] }

  describe 'on collection' do
    subject(:suppliers) { client.suppliers }

    it '.list', skip: 'Not available'

    describe '.create' do
      it 'returns a Hash' do
        @supplier = suppliers.create(supplier_data)

        expect(@supplier).to be_a(Hash)
      end

      it 'returns a Hash that represents a Supplier' do
        @supplier = suppliers.create(supplier_data)

        expect(@supplier.keys).to include(*supplier_keys)
      end

      it 'returns a persisted Supplier' do
        @supplier = suppliers.create(supplier_data)

        expect(@supplier['id']).not_to be_nil
      end

      it 'returns a persisted Supplier with correct data' do
        @supplier = suppliers.create(supplier_data)

        expect(@supplier['name']).to eq(supplier_data[:name])
      end
    end
  end

  describe 'on member' do
    subject(:supplier) { client.suppliers(@supplier['id']) }

    before { @supplier = client.suppliers.create(supplier_data) }

    describe '.get' do
      it 'returns a Hash' do
        expect(supplier.get).to be_a(Hash)
      end

      it 'returns a Hash that represents a Supplier' do
        expect(supplier.get.keys).to include(*supplier_keys)
      end

      it 'returns the requested Supplier' do
        expect(supplier.get['id']).to eq(@supplier['id'])
      end
    end

    describe '.update' do
      let(:update_data) { fake_supplier_data }

      it 'returns a Hash' do
        expect(supplier.update(update_data)).to be_a(Hash)
      end

      it 'returns a Hash that represents a Supplier' do
        expect(supplier.update(update_data).keys).to include(*supplier_keys)
      end

      it 'returns the same Supplier' do
        expect(supplier.update(update_data)['id']).to eq(@supplier['id'])
      end

      it 'returns an updated Supplier' do
        expect(supplier.update(update_data)['name']).to eq(update_data[:name])
      end
    end

    describe '.delete' do
      it 'deletes a Supplier' do
        supplier.delete

        # NOTE: Should raise a Faraday::ResourceNotFound exception but error handling is in progress
        expect { supplier.get }.to raise_error(Faraday::BadRequestError)
        remove_instance_variable(:@supplier)
      end
    end
  end
end
