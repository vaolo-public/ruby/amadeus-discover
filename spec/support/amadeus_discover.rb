# frozen_string_literal: true

require_relative 'helpers/amadeus_discover'

# Add project specific helpers in two versions:
#
# - One with mock and fake data for unit tests
# - One with real data (fetched from ENV is needed) for integration tests
#
RSpec.configure do |config|
  config.include Helpers::AmadeusDiscover::Unit, type: :unit
  config.include Helpers::AmadeusDiscover::Integration, type: :integration
end
