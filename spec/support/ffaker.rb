# frozen_string_literal: true

require 'ffaker'

require_relative 'helpers/fake'

# Configure FFaker to use the same random seed as Rspec
RSpec.configure do |config|
  config.before(:all) do
    FFaker::Random.seed = config.seed
  end

  config.before do
    FFaker::Random.reset!
    FFaker::UniqueUtils.clear
  end

  # Fake helpers are only available in integration tests
  config.include Helpers::Fake, type: :integration
end
