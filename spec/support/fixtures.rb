# frozen_string_literal: true

require_relative 'helpers/fixtures'

RSpec.configure do |config|
  config.extend Helpers::Fixtures::ClassMethods
end
