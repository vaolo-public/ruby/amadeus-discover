# frozen_string_literal: true

require 'hashie'
require_relative 'helpers/hashie'

RSpec.configure do |config|
  config.include Helpers::Hashie
end
