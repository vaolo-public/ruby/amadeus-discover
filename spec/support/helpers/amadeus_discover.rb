# frozen_string_literal: true

module Helpers
  module AmadeusDiscover
    module Unit
      def domain
        'https://example.com'
      end

      def credentials(base = {})
        { username: 'user',
          password: 'pass',
          client_id: 'content-partner-api',
          grant_type: 'password' }.merge(base)
      end

      def mock_authentication(host)
        self.class.fixtures :authenticator

        Typhoeus.stub("#{host}#{::AmadeusDiscover::Authenticator.const_get(:ENDPOINT)}")
                .and_return(json_response(200, authenticator_fixtures['200']))
      end
    end

    module Integration
      def domain
        ENV.fetch('AMADEUS_DISCOVER_HOST', nil)
      end

      def credentials(base = {})
        { username: ENV.fetch('AMADEUS_DISCOVER_USERNAME', nil),
          password: ENV.fetch('AMADEUS_DISCOVER_PASSWORD', nil),
          client_id: 'content-partner-api',
          grant_type: 'password' }.merge(base)
      end
    end
  end
end
