# frozen_string_literal: true

require 'securerandom'

module Helpers
  module Fake
    def fake_supplier_data
      {
        name: FFaker::Lorem.words.join(' '),
        externalId: SecureRandom.uuid,
        country: 'FR', # Should be FFaker::Address.country_code but not all country code are supported
        channel: 'ALENTOUR'
      }
    end

    def fake_product_data
      {
        title: {
          en: FFaker::Lorem.words.join(' '),
          es: FFaker::Lorem.words.join(' '),
          fr: FFaker::Lorem.words.join(' ')
        },
        externalId: SecureRandom.uuid,
        channel: 'ALENTOUR',
        bookingEngine: ENV.fetch('AMADEUS_DISCOVER_BOOKING_ENGINE', nil)
      }
    end
  end
end
