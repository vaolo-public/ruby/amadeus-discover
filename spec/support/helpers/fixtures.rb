# frozen_string_literal: true

require 'json'

module Helpers
  module Fixtures
    module ClassMethods
      def fixtures(*names)
        names.each { |name| fixture(name) }
      end

      def fixture(name)
        path = fixture_path(name)
        let(:"#{name}_fixtures") { JSON.parse(File.read(path)) }
      end

      protected

      # As this does not intent to be used in any inheritance chain, class
      # variables are fine here and are the easiest to implement.
      # rubocop:disable Style/ClassVars
      @@fixtures_path = File.join(__dir__, '/../../fixtures')
      # rubocop:enable Style/ClassVars

      def fixture_path(name)
        Pathname.new(File.join(@@fixtures_path, "#{name}.json")).realpath.to_s
      end
    end
  end
end
