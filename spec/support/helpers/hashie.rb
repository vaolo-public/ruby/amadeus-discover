# frozen_string_literal: true

module Helpers
  module Hashie
    def stringify_keys(hash)
      ::Hashie.stringify_keys hash
    end

    def symbolize_keys(hash)
      ::Hashie.symbolize_keys hash
    end
  end
end
