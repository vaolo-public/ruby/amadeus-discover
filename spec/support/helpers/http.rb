# frozen_string_literal: true

module Helpers
  module Http
    def http_connection(host)
      Faraday.new(host) do |builder|
        builder.adapter :typhoeus
      end
    end

    def http_response(code, body, headers = {})
      Typhoeus::Response.new(code: code, body: body, headers: headers)
    end

    def json_response(code, body)
      http_response(code, JSON.dump(body), { 'Content-Type' => 'application/json' })
    end

    def content_partner_credentials(username:, password:)
      {
        username: username,
        password: password,
        client_id: 'content-partner-api',
        grant_type: 'password'
      }
    end
  end
end
