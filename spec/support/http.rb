# frozen_string_literal: true

require 'faraday'
require 'faraday/typhoeus'

require_relative 'helpers/http'

# Ensure we clear Typhoeus stubs between tests
RSpec.configure do |config|
  config.include Helpers::Http

  config.before(:each, type: :unit) do
    Typhoeus::Expectation.clear
  end
end
