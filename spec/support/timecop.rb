# frozen_string_literal: true

require 'timecop'

# Turn on Timecop's safe mode
# Forces to use Timecop with the block syntax since it always puts time back the way it was.
Timecop.safe_mode = true
