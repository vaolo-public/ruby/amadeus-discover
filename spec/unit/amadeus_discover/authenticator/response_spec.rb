# frozen_string_literal: true

RSpec.describe AmadeusDiscover::Authenticator::Response, type: :unit do
  subject(:response) { described_class.new(args) }

  let(:access_token) { 'token' }
  let(:expires_in) { 60 }

  shared_examples 'a response wrapper' do
    it 'responds to :access_token' do
      expect(response).to respond_to(:access_token)
    end

    it 'returns expected value for :access_token' do
      expect(response.access_token).to eq(access_token)
    end

    it 'responds to :expires_in' do
      expect(response).to respond_to(:expires_in)
    end

    it 'returns expected value for :expires_in' do
      expect(response.expires_in).to eq(expires_in)
    end
  end

  context 'with a Hash with keys as symbols' do
    let(:args) { { access_token: access_token, expires_in: expires_in } }

    it_behaves_like 'a response wrapper'
  end

  context 'with a Hash with keys as strings' do
    let(:args) { { 'access_token' => access_token, 'expires_in' => expires_in } }

    it_behaves_like 'a response wrapper'
  end

  context 'with a Hash with extraneous keys' do
    let(:args) { { access_token: access_token, expires_in: expires_in, extra: 'anything' } }

    it_behaves_like 'a response wrapper'

    it 'ignores extraneous keys' do
      expect(response).not_to respond_to(:extra)
    end
  end

  context 'with a Hash with missing required keys' do
    let(:args) { { access_token: access_token } }

    it 'raises an exception' do
      expect { described_class.new(args) }.to raise_error(ArgumentError)
    end
  end
end
