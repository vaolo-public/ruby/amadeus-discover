# frozen_string_literal: true

RSpec.describe AmadeusDiscover::Authenticator::Token, type: :unit do
  subject(:token) { described_class.new(response) }

  let(:response) { { access_token: 'token', expires_in: 60 } }

  describe '.access_token' do
    it 'returns expected value for :access_token' do
      expect(token.access_token).to eq(response[:access_token])
    end

    it 'is aliased as :value' do
      expect(token.value).to eq(response[:access_token])
    end
  end

  describe '.expires_in' do
    it 'returns expected value for :expires_in' do
      expect(token.expires_in).to eq(response[:expires_in])
    end
  end

  describe '.issued_at' do
    it 'uses now as a default value' do
      Timecop.freeze do
        token = described_class.new(response)
        expect(token.issued_at).to eq(Time.now)
      end
    end

    it 'uses specific value if provided on creation' do
      Timecop.freeze do
        token = described_class.new(response, Time.now - 10)
        expect(token.issued_at).to eq(Time.now - 10)
      end
    end
  end

  describe '.valid?' do
    it 'is valid until expiration' do
      expect(token).to be_valid
    end

    it 'is invalid once expired' do
      token = described_class.new(response)
      Timecop.travel(token.issued_at + response[:expires_in] + 1) do
        expect(token).not_to be_valid
      end
    end
  end
end
