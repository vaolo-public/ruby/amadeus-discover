# frozen_string_literal: true

RSpec.describe AmadeusDiscover::Authenticator, type: :unit do
  subject(:authenticator) { described_class.new(connection, credentials) }

  # Load response fixtures from spec/fixtures/authenticator.json and make them
  # available as authenticator_fixtures (same as let(:authenticator_fixtures) { … })
  fixtures :authenticator

  let(:connection) { http_connection(domain) }
  let(:endpoint) { described_class.const_get(:ENDPOINT) }

  context 'when credentials are invalid' do
    it 'raise an exception' do
      expect { described_class.new(connection, credentials.slice(:username)) }.to raise_error(ArgumentError)
    end
  end

  describe '.bearer_token' do
    before { Typhoeus.stub("#{domain}#{endpoint}").and_return(response) }

    context 'when everything is fine' do
      let(:token) { authenticator_fixtures['200']['access_token'] }
      let(:response) { json_response(200, authenticator_fixtures['200']) }

      it 'requests authentication URL' do
        allow(connection).to receive(:post).with(/#{endpoint}\z/, anything).and_call_original
        authenticator.bearer_token
        expect(connection).to have_received(:post).with(/#{endpoint}\z/, anything).exactly(1).time
      end

      it 'requests authentication token with given credentials' do
        allow(connection).to receive(:post).with(anything, stringify_keys(credentials)).and_call_original
        authenticator.bearer_token
        expect(connection).to have_received(:post).with(anything, stringify_keys(credentials)).exactly(1).time
      end

      it 'returns a token' do
        expect(authenticator.bearer_token).to be_an(String)
      end

      it 'returns the expected token' do
        expect(authenticator.bearer_token).to eq(token)
      end

      context 'with multiple requests' do
        before { authenticator.bearer_token }

        it 'keeps the same token if valid' do
          allow(connection).to receive(:post).and_call_original
          authenticator.bearer_token
          expect(connection).not_to have_received(:post)
        end

        it 'fetches a new token if expired' do
          allow(connection).to receive(:post).and_call_original
          Timecop.travel(Time.now + (60 * 60) + 1) { authenticator.bearer_token }
          expect(connection).to have_received(:post)
        end
      end
    end

    context 'when response is invalid' do
      let(:response) { json_response(200, authenticator_fixtures['200'].slice('expires_in')) }

      it 'raise an exception' do
        expect { authenticator.bearer_token }.to raise_error(Faraday::ParsingError)
      end
    end

    context 'when credentials are invalid' do
      let(:response) { json_response(401, authenticator_fixtures['401']) }

      it 'raise an exception' do
        expect { authenticator.bearer_token }.to raise_error(Faraday::UnauthorizedError)
      end
    end
  end
end
