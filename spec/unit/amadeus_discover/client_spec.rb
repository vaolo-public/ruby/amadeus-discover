# frozen_string_literal: true

require 'json'

RSpec.describe AmadeusDiscover::Client, type: :unit do
  before { mock_authentication(domain) }

  fixtures :client

  let(:connection) { http_connection(domain) }

  shared_examples 'an API client' do
    context 'with a dummy namespace' do
      subject(:response) { client.post(data) }

      before do
        client.singleton_class.include namespace
        Typhoeus.stub("#{domain}/api/dummy/").and_return(response_stub)
      end

      let(:response_stub) { json_response(200, client_fixtures['200']) }
      let(:data) { { a: 1 } }
      let(:namespace) do
        Module.new do
          def post(data)
            connection.post('/api/dummy/', data)
          end
        end
      end

      it 'sets a custom User-Agent with gem name and version' do
        expect(response.env.request_headers[:user_agent]).to match(%r{AmadeusDiscover/#{AmadeusDiscover::VERSION}})
      end

      it 'sets a custom User-Agent with Ruby version' do
        expect(response.env.request_headers[:user_agent]).to match(%r{Ruby/#{RUBY_VERSION}})
      end

      it 'sends an authorization token' do
        expect(response.env.request_headers[:authorization]).to match(/Bearer/)
      end

      it 'sends request as JSON' do
        expect(response.env.request_headers[:content_type]).to match(%r{application/json})
      end

      it 'sends request body as JSON' do
        expect(response.env.request_body).to eq(JSON.dump(data))
      end

      it 'parses response body as JSON' do
        expect(response.body).to be_a(Hash)
      end

      context 'when something goes wrong' do
        let(:response_stub) { json_response(500, client_fixtures['500']) }

        it 'raises an error' do
          expect { response }.to raise_error(Faraday::ServerError)
        end
      end
    end

    describe '.suppliers' do
      context 'without arguments' do
        it 'returns the correct collection namespace' do
          expect(client.suppliers).to be_a(AmadeusDiscover::Namespaces::Suppliers::Collection)
        end

        it 'returns a namespace set with the correct client' do
          expect(client.suppliers.client).to eq(client)
        end
      end

      context 'with a supplier_id' do
        let(:supplier_id) { 12 }

        it 'returns the correct member namespace' do
          expect(client.suppliers(supplier_id)).to be_a(AmadeusDiscover::Namespaces::Suppliers::Member)
        end

        it 'returns a namespace set with the correct client' do
          expect(client.suppliers(supplier_id).client).to eq(client)
        end

        it 'returns a namespace with memoized supplier_id' do
          expect(client.suppliers(supplier_id).supplier_id).to eq(supplier_id)
        end
      end
    end
  end

  context 'with explicit arguments' do
    subject(:client) { described_class.new(connection, credentials) }

    it_behaves_like 'an API client'
  end

  context 'with a global configuration' do
    subject(:client) { described_class.new }

    around do |example|
      saved_connection = AmadeusDiscover::Config.connection
      saved_credentials = AmadeusDiscover::Config.credentials

      AmadeusDiscover.configure do |config|
        config.connection = connection
        config.credentials = credentials
      end

      begin
        example.run
      ensure
        AmadeusDiscover.configure do |config|
          config.connection = saved_connection
          config.credentials = saved_credentials
        end
      end
    end

    it_behaves_like 'an API client'
  end
end
