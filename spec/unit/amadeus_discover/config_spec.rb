# frozen_string_literal: true

RSpec.describe AmadeusDiscover::Config, type: :unit do
  subject(:config) { described_class }

  around do |example|
    saved_connection = AmadeusDiscover::Config.connection
    saved_credentials = AmadeusDiscover::Config.credentials

    begin
      example.run
    ensure
      AmadeusDiscover.configure do |config|
        config.connection = saved_connection
        config.credentials = saved_credentials
      end
    end
  end

  it 'stores credentials' do
    expect(config).to respond_to(:credentials)
  end

  it 'allows to set credentials' do
    expect(config).to respond_to(:credentials=)
  end

  describe '.credentials' do
    it 'stores credentials as a Hash' do
      expect(config.credentials).to be_a(Hash)
    end

    it 'has no credentials by default' do
      expect(config.credentials).to be_empty
    end

    it 'returns credentials when set' do
      credentials = { username: 'user', password: 'password' }
      config.credentials = credentials
      expect(config.credentials).to eq(credentials)
    end
  end

  it 'stores connection' do
    expect(config).to respond_to(:connection)
  end

  it 'allows to set connection' do
    expect(config).to respond_to(:connection=)
  end

  describe '.connection' do
    it 'has no connection by default' do
      expect(config.connection).to be_nil
    end

    it 'returns connection when set' do
      connection = double('Faraday::Connection')
      config.connection = connection
      expect(config.connection).to eq(connection)
    end
  end
end
