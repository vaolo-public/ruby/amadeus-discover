# frozen_string_literal: true

RSpec.describe AmadeusDiscover::Namespace, type: :unit do
  subject(:decorator) { described_class.new(client) }

  describe '.client' do
    let(:client) { instance_double('AmadeusDiscover::Client') }

    it 'returns the client' do
      expect(decorator.client).to eq(client)
    end
  end

  describe '.connection' do
    let(:client) { AmadeusDiscover::Client.new(connection, credentials) }
    let(:connection) { http_connection(domain) }

    it 'returns a Faraday::Connection' do
      expect(decorator.connection).to be_a(Faraday::Connection)
    end

    it 'returns the correct connection' do
      expect(decorator.connection).to eq(client.connection)
    end
  end
end
