# frozen_string_literal: true

RSpec.shared_examples 'a client namespace operation' do |http_verb, params = nil|
  before do
    mock_authentication(domain)
    Typhoeus.stub("#{domain}#{endpoint}").and_return(response)
  end

  let(:response) { json_response(200, {}) }

  it 'executes a request with the correct method' do
    allow(client.connection).to receive(http_verb).and_call_original
    call_method(params)
    expect(client.connection).to have_received(http_verb).once
  end

  if params.nil?
    it 'requests the correct endpoint' do
      allow(client.connection).to receive(http_verb).with(endpoint).and_call_original
      call_method(params)
      expect(client.connection).to have_received(http_verb).with(endpoint)
    end
  else
    it 'requests the correct endpoint' do
      allow(client.connection).to receive(http_verb).with(endpoint, anything).and_call_original
      call_method(params)
      expect(client.connection).to have_received(http_verb).with(endpoint, anything)
    end

    it 'pass the correct parameters' do
      allow(client.connection).to receive(http_verb).with(anything, params).and_call_original
      call_method(params)
      expect(client.connection).to have_received(http_verb).with(anything, params)
    end
  end

  context 'on failed authentication' do
    let(:response) { json_response(401, {}) }

    it 'raises an exception' do
      expect { call_method(params) }.to raise_error(Faraday::UnauthorizedError)
    end
  end

  context 'on server error' do
    let(:response) { json_response(500, {}) }

    it 'raises an exception' do
      expect { call_method(params) }.to raise_error(Faraday::ServerError)
    end
  end

  protected

  def call_method(params)
    params.nil? ? method.call : method.call(params)
  end
end
