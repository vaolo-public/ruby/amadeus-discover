# frozen_string_literal: true

require './spec/unit/amadeus_discover/namespaces/shared/namespace'

RSpec.describe AmadeusDiscover::Namespaces::Suppliers::Products, type: :unit do
  let(:client) { AmadeusDiscover::Client.new(connection, credentials) }
  let(:connection) { http_connection(domain) }
  let(:supplier) { AmadeusDiscover::Namespaces::Suppliers::Member.new(client, supplier_id) }
  let(:supplier_id) { 3 }

  describe 'on collection' do
    let(:products) { AmadeusDiscover::Namespaces::Suppliers::Products::Collection.new(supplier) }

    describe '.list' do
      subject(:method) { -> { products.list } }

      let(:endpoint) { "/api/supplier/#{supplier_id}/products" }

      it_behaves_like 'a client namespace operation', :get
    end

    describe '.create' do
      subject(:method) { ->(params) { products.create(params) } }

      let(:endpoint) { "/api/supplier/#{supplier_id}/product" }

      it_behaves_like 'a client namespace operation', :post, {}
    end
  end

  describe 'on member' do
    let(:product) { AmadeusDiscover::Namespaces::Suppliers::Products::Member.new(supplier, product_id) }
    let(:endpoint) { "/api/supplier/#{supplier_id}/product/#{product_id}" }
    let(:product_id) { 12 }

    it '.get', skip: 'Not available'

    describe '.update' do
      subject(:method) { ->(params) { product.update(params) } }

      it_behaves_like 'a client namespace operation', :post, {}
    end

    describe '.delete' do
      subject(:method) { -> { product.delete } }

      it_behaves_like 'a client namespace operation', :delete
    end
  end
end
