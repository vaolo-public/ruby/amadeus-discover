# frozen_string_literal: true

require './spec/unit/amadeus_discover/namespaces/shared/namespace'

RSpec.describe AmadeusDiscover::Namespaces::Suppliers, type: :unit do
  let(:client) { AmadeusDiscover::Client.new(connection, credentials) }
  let(:connection) { http_connection(domain) }

  describe 'on collection' do
    let(:suppliers) { AmadeusDiscover::Namespaces::Suppliers::Collection.new(client) }
    let(:endpoint) { '/api/supplier' }

    it '.list', skip: 'Not available'

    describe '.create' do
      subject(:method) { ->(params) { suppliers.create(params) } }

      it_behaves_like 'a client namespace operation', :post, {}
    end
  end

  describe 'on member' do
    let(:supplier) { AmadeusDiscover::Namespaces::Suppliers::Member.new(client, id) }
    let(:endpoint) { "/api/supplier/#{id}" }
    let(:id) { 12 }

    describe '.get' do
      subject(:method) { -> { supplier.get } }

      it_behaves_like 'a client namespace operation', :get
    end

    describe '.update' do
      subject(:method) { ->(params) { supplier.update(params) } }

      it_behaves_like 'a client namespace operation', :post, {}
    end

    describe '.delete' do
      subject(:method) { -> { supplier.delete } }

      it_behaves_like 'a client namespace operation', :delete
    end

    describe '.products' do
      context 'without arguments' do
        it 'returns the correct collection namespace' do
          expect(supplier.products).to be_a(AmadeusDiscover::Namespaces::Suppliers::Products::Collection)
        end
      end

      context 'with a product_id' do
        let(:product_id) { 12 }

        it 'returns the correct member namespace' do
          expect(supplier.products(product_id)).to be_a(AmadeusDiscover::Namespaces::Suppliers::Products::Member)
        end

        it 'returns a namespace with memoized product_id' do
          expect(supplier.products(product_id).product_id).to eq(product_id)
        end
      end
    end
  end
end
