# frozen_string_literal: true

RSpec.describe AmadeusDiscover, type: :unit do
  it 'should have a version' do
    expect(AmadeusDiscover::VERSION).not_to be nil
  end

  it 'should have a version that matches the gemspec' do
    gemspec = Gem::Specification.load('amadeus-discover.gemspec')
    expect(AmadeusDiscover::VERSION).to eq(gemspec.version.to_s)
  end
end
