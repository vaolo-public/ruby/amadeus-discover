# frozen_string_literal: true

RSpec.describe AmadeusDiscover, type: :unit do
  describe '.configure' do
    it 'yields the AmadeusDiscover::Config module' do
      described_class.configure do |config|
        expect(config).to eq(AmadeusDiscover::Config)
      end
    end
  end
end
